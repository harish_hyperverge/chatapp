const express = require('express')
const router = express()
const cors = require('cors')
router.use(cors())
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
const path = require('path')
dotenv.config({path: path.resolve(__dirname, '../.env')})

router.use(bodyParser.urlencoded({
  extended: false,
  limit: '50mb'
}))
router.use(bodyParser.json({limit: '50mb'}))

router.get('/', (req, res) => {
  res.send('Welcome to Chat App')
})


router.use('/auth', require('./Auth/authController'))
router.use('/chat', require('./Chat/chatController'))
router.use('/user', require('./User/userController'))



module.exports = router