const userModel = require('../models/user.model')
const messageModel = require('../models/message.model')
const ObjectId = require('mongodb').ObjectID;
const bcrypt = require('bcryptjs')
class QueryHandler{


	getLagdetails(userId,friendId,lastTimeStamp){
		return new Promise( async (resolve, reject) => {
			try {
				await messageModel.find({senderId : {$in : [ ObjectId(userId),ObjectId (friendId)] },
									receiverId : {$in : [ ObjectId(userId),ObjectId (friendId)] },
									isDeleted: false,
									"updatedAt" : { $gte : lastTimeStamp }}).sort({ sentAt: 1 }).exec((error, result) => { //updatedAt : {$gte : lastTimeStamp}
					if( error ){
						reject(error);
					}
					else{
						resolve(result.length !==0? result: []);
					}
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
	// get Last Timestamp of a particular User
	getLasttimestamp(userId){
		return new Promise( async (resolve, reject) => {
			try {
				await messageModel.find({
					senderId :  userId
				}).sort({updatedAt: -1}).limit(1).exec((error, result) => {
					if( error ){
						reject(error);
					}
					else{
						resolve(result.length !==0? result[0].updatedAt: null);
					}
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
	//Query to get recent Chats list.
	recentChats(userId){
		return new Promise( async (resolve, reject) => {
			try {
					const onefriendIds = await messageModel.distinct("senderId",{'receiverId': userId})
					const twofriendIds = onefriendIds.concat(await messageModel.distinct("receiverId",{'senderId': userId}))
					const userDetails = await userModel.find({"_id" : { $in : twofriendIds } })
					userDetails.sort((x, y) =>y.updatedAt - x.updatedAt)
					const chatListDetails = userDetails.map(async (item)=>{
					const unreadCount = await messageModel.countDocuments({'senderId':item._id,'receiverId': ObjectId(userId),'seenAt': null,'isDeleted': false})
							return {
								friendId: item._id,
								firstName: item.firstName,
								lastName: item.lastName,
								unreadCount: unreadCount,
								friendStatus: item.isOnline,
								profilePic: item.profilePic
							}
						})
					resolve(Promise.all(chatListDetails))
			} catch (error) {
				reject(error)
			}
		});
	}

	//Query to insert new message in message model.
    sendNewMessage(body){
		return new Promise( (resolve,reject) => {
				const { senderId, receiverId, message, isAttachment} = body;
				const newMessage = new messageModel({
					senderId,
					receiverId,
					message,
					isAttachment
					})
				    newMessage.save((error,result) => {
					 if(error){
						 reject(error)
					 }
					 else{
						 resolve(result)
					 }
				 })
		})
	}

	//search Friends name in Chat Screen
	searchUserNames(name){
		return new Promise((resolve, reject) => {
			try {
				userModel.find({
					$or: [ { 'firstName':{ $regex: '.*' + name + '.*' } },{ 'lastName':{ $regex: '.*' + name + '.*' } } ]
				}, (err, result) => {
                    if( err ){
						reject(err);
					}
					resolve(result);
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
    
    //Query to update users isOnline status to true 
	makeUserOnline(userId){
		return new Promise(  (resolve, reject) => {
			try {
				userModel.findByIdAndUpdate({
					_id : ObjectId(userId)
				},{ "$set": {'isOnline': true} },{new: true}, (err, result) => {
					if( err ){
						reject(err);
					}
					resolve(result);
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
	makeUserOffline(userId){
		return new Promise(  (resolve, reject) => {
			try {
				userModel.findByIdAndUpdate({
					_id : ObjectId(userId)
				},{ "$set": {'isOnline': false} }, (err, result) => {
					if( err ){
						reject(err);
					}
					resolve(result);
				});
			} catch (error) {
				reject(error)
			}	
		});
	}

	//query to find and return the row in users model with email id.
    getUserByEmail(e){
		return new Promise((resolve, reject) => {
			try {
				userModel.find({
					email :  e
				}, (error, result) => {
					if( error ){
						reject(error);
					}
					resolve(result[0]);
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
	//register new user into system.
	registerNewUser(body){
		return new Promise( (resolve,reject) => {
			const { firstName, lastName, email, password, phone, profilePic  } = body;
			const data= profilePic!== '' ?{
				firstName,
				lastName,
				email,
				password,
				phone,
				profilePic
			}: {
				firstName,
				lastName,
				email,
				password,
				phone
			}
			const userReg = new userModel(data)
				userReg.save((error,result) => {
				 if(error){
					 reject(error)
				 }
				 else{
					 resolve(result)
				 }
			 })
	})
	}
	
	//get user details from phone or email for unique constraint check
	checkUniqueEmailPhone(email, phone){
		return new Promise(  (resolve, reject) => {
			try {
				userModel.find({
					$or: [ { 'email':email },{ 'phone':phone } ] 
				}, (error, result) => {
					if( error ){
						reject(error);
					}
					resolve(result);
				});
			} catch (error) {
				reject(error)
			}	
		});
	}
	//Query to get chat between two users
	getChat(userId,friendId) {
		return new Promise ( (resolve,reject) => {
			try {
				const chat = messageModel.find({senderId : {$in : [ ObjectId(userId),ObjectId (friendId)] },
							receiverId : {$in : [ ObjectId(userId),ObjectId (friendId)] },
							isDeleted: false
				} ).sort( { sentAt: 1 } )
				resolve(chat)
			} catch (error) {
				reject(error)
			}
		})
	}

	getUserById(userId) {
		return new Promise ( (resolve,reject) => {
			try {
				const userDetails = userModel.findOne({_id: ObjectId(userId)})
				resolve(userDetails)
			} catch (error) {
				reject(error)
			}
		})
	}

	editMessage(messageId,content){
		return new Promise( (resolve,reject) => {
			try{
				const msgUpdate = messageModel.findOneAndUpdate({_id : ObjectId(messageId)},
					{ "$set": {'isEdited': true, message: content} },{
						new: true})
				resolve(msgUpdate)
			} catch (error) {
				reject(error)
			}
		})
	}

	deleteMessage(messageId){
		return new Promise( (resolve,reject) => {
			try{
				const msgUpdate = messageModel.findOneAndUpdate({_id : ObjectId(messageId), isDeleted: false},
					{ "$set": {'isDeleted': true} },{
						new: true})
				resolve(msgUpdate)
			} catch (error) {
				reject(error)
			}
		})
	}

	updateMessageInfo(messageIds,isDelivered,isSeen){
		return new Promise( (resolve,reject) => {
			try{
				if(isDelivered && isSeen ){
					const msgUpdate = messageModel.updateMany({_id : { $in : messageIds }},
						{ "$set": {'seenAt': Date.now() , 'deliveredAt': Date.now()} })
					resolve(msgUpdate)
				} else if (isDelivered) {
					const msgUpdate = messageModel.updateMany({_id : { $in : messageIds }},
						{ "$set": {'deliveredAt': Date.now()} })
					resolve(msgUpdate)
				} else if (isSeen) {
					const msgUpdate = messageModel.updateMany({_id : { $in : messageIds }},
						{ "$set": {'seenAt': Date.now() } })
					resolve(msgUpdate)
				}
			} catch (error) {
				reject(error)
			}
		})
	}

	updateDeliveredAt(userId,friendIds){
		return new Promise(async (resolve,reject) => {
			try{
				const msgUpdate = await messageModel.updateMany({senderId : { $in : friendIds },
					deliveredAt: { $exists: false }, receiverId:ObjectId(userId)},
					{ "$set": {'deliveredAt': Date.now() } },{ upsert: true })
					console.log('d',msgUpdate);
				resolve(msgUpdate)
			} catch (error) {
				reject(error)
			}
		})
	}
}

module.exports = new QueryHandler();
