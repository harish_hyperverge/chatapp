const express = require('express')
const { check, validationResult } = require('express-validator')
const user = express()
const jwtToken = require('../../middleware/jwtToken')
const query = require('../queries')
const mongoose = require('mongoose')

// @route GET   /user/searchFriends/:searchName
// @desc        Search Friend's name
// @access      Private
user.get('/searchFriends/:searchName',
 check('seachName','Please provide certain name').notEmpty(),
 jwtToken,
 async (request,response)=>{
    const errors = validationResult(request.params);
    if (!errors.isEmpty()) {
        return response.status(400).send(errors.array()[0])
    }
    else{
        const name = request.params.searchName
        const userList = await query.searchUserNames(name)
        const friendList = userList.map((item)=>{
            if (String(item._id) !== String(request.token.id)){
                return {
                    friendId: item._id ,
                    firstName: item.firstName,
                    lastName: item.lastName,
                    profilePic: item.profilePic 
                }
            }
        })
        response.status(200).send({
            msg: "List found",
            auth : true,
            data : friendList.length !== 0 ? friendList.filter((item) => item != null) : []
        })
    }
 }
)

// @route POST   /user/recentChats
// @desc        Get Recent Chat list
// @access      Private
user.post('/recentChats',
 check('userId','Please provide userId').notEmpty(),
 jwtToken,
 async (request,response)=>{
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).send(errors.array()[0])
    }
    else{
        const userId = request.body.userId
        const userIdCheck = mongoose.isValidObjectId(userId)
        if (userIdCheck){
            const recentChatList = await query.recentChats(userId)
            response.status(200).send({
                msg: "List found",
                auth : true,
                lastTimestamp : await query.getLasttimestamp(userId),
                data : {
                    totalNewChats : recentChatList.filter((obj) => obj.unreadCount !== 0).length,
                    recentChats :recentChatList
                }
            })
        }
    }

})

module.exports = user 