const mongoose = require('mongoose')
const dotenv = require('dotenv')
const path = require('path')
dotenv.config({path: path.resolve(__dirname, '../../.env')})

mongoose.connect(process.env.mongodbURL, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }, (err) => {
  if (!err) {
    console.log('Connection Success!!')
  } else {
    console.log('Error connecting :(')
  }
})
