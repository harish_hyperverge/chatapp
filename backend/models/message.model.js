const mongoose = require('mongoose')

const messageSchema = new mongoose.Schema({
    senderId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'users'
    },
    receiverId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'users'
    },
    isAttachment: {
        type: Boolean,
        required: true,
        default: false
    },
    message: {
        type: String,
        required : true
    },
    isEdited: {
        type: Boolean,
        required: true,
        default: false
    },
    isDeleted: {
        type: Boolean,
        required: true,
        default: false
    },
    sentAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    deliveredAt: {
        type: Date
    },
    seenAt: {
        type: Date
    }
},
{
  timestamps: true
})

module.exports = mongoose.model('messages',messageSchema)