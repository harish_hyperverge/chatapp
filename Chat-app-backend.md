# ChatApp - Back-end Documentation

## 1.0 Data Structure Design 
### 1.1 DataBase structure
The Database used is mongodb for ease of storing any message /document varying in length and type.  

It will have 2 models.

### 1.1.1 User
| Attribute Name | Attribute Type | 
| :------------- | :------------  |
_id              |  Object
firstName        |  String
lastName         |  String
phone            |  Number
email            |  String
password         |  String
profilePic       |  String
isOnline         |  Boolean
createdAt        |  Date
updatedAt        |  Date

### 1.1.2 Messages

| Attribute Name | Attribute Type| 
| :------------  | :-----------  |
_id              |  Object
senderId         |  Object
receiverId       |  Object
isAttachment     |  Boolean
message          |  String
isEdited         |  Boolean
isDeleted        |  Boolean
sentAt           |  Date
deliveredAt      |  Date
seenAt           |  Date
createdAt        |  Date
updatedAt        |  Date

### 1.2 Server Socket Structure
All the sockets connected to the clients will be stored in an object in following format:
        
        [
        	{
        		userId: ‘yyy’,
        		socketIds: [‘yyyyyy’,’cccc’,’mmmm’],
        		lastTimeStamp : timestamp
            },
            {
        		userId: ‘xxxxxx’,
        		socketIds: [‘xx’,’yy’,’zz’],
        		lastTimeStamp : timestamp
            }
        ]
        
```lastTimeStamp is the maxinmum of updatedAt for any user.```

Socket Listeners - connection, messages, friendStatus, recentChat, typing, disconnect

**Socket Listeners description**:  
1. connection:  
This socket listener will check for new user in a socket, that will add userId and socketId(in socketIds array) in socket object. If userId already exists then will add socketId in that userId's socket object. Update particular user's status in DB if the userId is new to the socket object.
2. messages: 
This socket listener will broadcast the message updates.
3. friendStatus:
This socket listener will emit user's status to client whenever a user connected or disconnected.
4. recentChat:
This socket listener will emit the updated chat list of a user.
5. typing:
This socket listener will listen for the keypress event from the client and send back a broadcast to the receiver.
6. disconnect:
This socket listener will remove the socketId from the socket object for a particular user once he/she disconnects.Update particular user's status in DB if userId does not exists in socket object.

Before any transaction - the client’s and server’s last time stamp for the logged in user will be matched. If both are equal, then go forward with the transaction, otherwise broadcast all the transactions which happened in the interval between the time stamps.


![Socket Flow](https://drive.google.com/uc?export=view&id=1LalhB2hgEeTZ2m17prGGEHfX8U3HmkbS)

## 2.0 APIs :
Following are all the backend APIs with their method types, payload details and the responses.

### Status Code table:

|       Code      |     Description       |
|:----------------|:----------------------|
200               |         Ok            |
400               |     Bad Request       |
401               |     Unauthorized      |
404               |     Not Found         |
409               |      Conflict         |
500               | Internal Server Error |


### 2.1 auth/register/
***Description***:  
This API is used to register new user's details.
```json
Method : POST,
Headers: {
    “content-type” : “application/json”
},
Body: {
    “firstName” : “XXXXX”,
    “lastName” : “YYYYY”,
    “phone”  : ”9999999999”,
    “email”  : ”xxxx@xxx.yyy”,
    “password”  : “wwwwww”,
    “profilePic” : base64(img)
}

API response:
Status code: 200 / 500 / 400 / 409
{
    “msg” :  “Registered / Bad Payload / Not Registered / DB error / Conflict ”
}
```
### 2.2 auth/login/
***Description***:  
This API is used to validate the login credentials and provide with authenticated token(JWT).
```json
Method: POST,
Headers: {
    “content-type” : “application/json”
},
Body: {
    “email”  : "xxxx@xxx.yyy",
    “password”  : “wwwwww”
}

API response:
Status code: 200 / 500 / 400 / 404
{
    “msg” :  “Logged in / Bad Payload / Try Again / DB error / User not found”,
    “auth” : true / false 
    “token” : ( JWT )
    “data” : {
        “loggedData” : {
                        “userId” : db.data._id,
                        “firstName” : db.data.firstName,
                        “lastName” : db.data.lastName,
                        “email” : xxxx@xx.yyy,
                        “profilePic” : base64(img)
                        }
                }
}
```  
### 2.3 user/searchFriends/:searchName
***Description***:  
This API is used to get list of users which matches the passed searchName.

```json
Method: GET,
Headers: {
    “content-type” : “application/json”,
    “x-access-token”: (JWT),
    “lastTimestamp” : timestamp
},
Params: {
    “searchName”  : XXXXX
}

API response:
Status code: 200 / 500 / 400 / 401 / 404
{
    “msg” :  “Bad Payload / Try Again / DB error / Name doesn’t match”,
    “auth” : true / false,
    “lastTimestamp” : timestamp,
    “data” :[
                {
                    “friendId” : db.data._id,
                    “firstName” : db.data.firstName,
                    “lastName” : db.data.lastName,
                    “profilePic” : base64(img),
                ...
                },{...},
                ….
            ]
}
```
### 2.4 chat/activeChat/
***Description***:  
This API is used to get list of previous messages in a chat, new messages in a chats and count of new messages in a chat with particular user.

```json
Method: POST,
Headers: {
    “content-type” : “application/json”,
    “x-access-token”: (JWT),
    “lastTimestamp” : timestamp
},
Body : {
    “friendId”  : "XXXXX",
    “userId” : "XXXXXX"
}

API response:
Status code: 200 / 500 / 400 / 401 / 404
{
    “msg” :  “Bad Payload / Try Again / DB error / User Not Found”,
    “auth” : true / false,
    “lastTimestamp” : timestamp,
    “data” : {
        “friendId” : db.data._id,
        “friendName” : XXXXX,
        “friendStatus” : Online/Offline,
                        “profilePic” : base64(img),
        “lastSeenAt” : Date,
        “unreadCount” : XX,
        “previousMessages” :[
                                {
                                “senderId” : db.data._id,
                                “receiverId” : db.data._id,
                                “messageId” : db.data._id,
                                “message” : xxxxxxxxxxxxx
                                “sentAt” : Date,
                                “deliveredAt” : Date,
                                “seenAt” : Date,
                                “isEdited” : true/False,
                                “isDeleted”: true/False,
                                “isAttachment” : true/False
                                },{...},
                                ….
                            ]
        }
}
``` 
### 2.5 user/recentChats/
***Description***:  
This API is used to get list of previous chats, incoming chats and count of new Chats.

```json
Method: POST,
Headers: {
    “content-type” : “application/json”,
    “x-access-token”: (JWT),
    “lastTimestamp” : null
},
Body: {
    “userId”  : db.data._id
}

API response:
Status code: 200 / 500 / 400 / 401 / 404
{
    “msg” :  “Bad Payload / Try Again / DB error / User Not Found”,
    “auth” : true / false,
    “lastTimestamp” : timestamp,
    “data” : {
        “totalNewChats” : XX
        “recentChats” :[
                            {
                            “friendId” : db.data._id,
                            “firstName” : db.data.firstName,
                            “lastName” : db.data.lastName,
                            “unreadCount” : XX,
                            “friendStatus” : Online/ Offline,
                            “profilePic” : base64(img)
                            },{...},
                            ….
                        ]
        }
}
```
### 2.6 chat/update/
***Description***:   
This API is used to update the chat by adding new message in a chat, editing a previous message in a chat and deleting a message from a chat of any user.

```json
Method: POST,
Headers: {
    “content-type” : “application/json”,
    “x-access-token”: (JWT),
    “lastTimestamp” : timestamp
},
Body: {
    “messageId” : db.data._id,
    “senderId”  : db.data._id,
    “receiverId”  : db.data._id,
    “message” :  xxxxxxxxxxxx,
    “isAttachment” : true/false,
    “action“: “NEW / EDIT / DELETE“
}

API response:
Status code: 200 / 500 / 400 / 401 / 404
{
    “msg” :  “Bad Payload / Try Again / DB error / User not found/ Message Sent / Edited / Deleted”,
    “auth” : true / false,
    “lastTimestamp” : timestamp,
    “action“: “NEW / EDIT / DELETE“, 
    “data” :{
                “senderId” : db.data._id,
                “receiverId” : db.data._id,
                “messageId” : db.data._id,
                “message” : xxxxxxxxxxxxx
                “sentAt” : Date,
                “deliveredAt” : Date,
                “seenAt” : Date,
                “isEdited” : true/false,
                “isDeleted”: true/false,
                “isAttachment” : true/false
            }
    }
}
```
### 2.7 chat/updateMessageInfo/
***Description***:  
This API is used to update the message's informations such as message deliveredAt and message seenAt.
  
```json
Method: PUT,
Headers: {
    “content-type” : “application/json”,
    “x-access-token”: (JWT),
    “lastTimestamp” : timestamp
},
Body:{
        “messageIds” : [db.data._id]
        “isDelivered” : true,
        “isRead” : true 
}

API response:
Status code: 200 / 500 / 400 / 401 / 404
{
    “msg” :  “Bad Payload / Try Again / DB error / User not found”,
    “auth” : true / false,
    “messageIds” : [db.data._id]
    “deliveredAt” : deliveredAt,
    “seenAt” : seenAt,
    “lastTimestamp” : timestamp
}
```






