# Chat App - Front-end Documentation

## 1.0 Introduction
### 1.1 Purpose
This Software Design Document provides the design details of the Chat application which is to be developed using node.js, socket.io, mongodb and react/redux.  

This application will be used inside a particular organization to share information along with some attachments in it.
### 1.2 Scope
This document contains a complete description of the design of the Chat application.  

The basic architecture is a web server from a client server paradigm. The basic pages will be in JS components. Application developers will have full access to the mongo database, Backend and Frontend code.  

The users of this application can be able to view the friends list with whom they already had a chat, they can also search for a new user’s name to start a chat. In the chat box users can see the entire chat thread between them. They also have options like send message, edit message, delete message, view particular message details like delivered timestamp, read timestamp.

## 2.0 Web System Flow
### 2.1 Flow Diagram

![App Flow](https://drive.google.com/uc?export=view&id=1bufY8dswOSF3MJUFTCzz_7OnMGJgrWph) 

### 2.1.1 Registration Page:
**Name**: User Registration Page  
**Type**: Web Page  
**Description**:  
This is the page where the new user will register him/herself into this application. In top Nav bar there will be a Button ie. Login button, if the user has  already registered, He/She can go to the login page to enter into the application. In order to register, user needs to fill in following details: 
1. First Name 
2. Last Name 
3. Phone 
4. Email 
5. Password 
6. Profile Image.

User needs to click on submit to register on the app.  
**Operations**:  
**Name**: submitRegistration()  
**API**: /auth/register  
**Arguments**:  

|      Fields       |      type      |
|:------------------|:--------------:|
firstName           | string
lastName            | string
phone               | string
email               | string
password            | string
profileImage        | string
isOnline            | boolean

**Pre-condition**: 
* All fields are filled.
* Password should be of min length 8
* Phone number should be of length 10

**Returns**:   
- Message from API response. 

**Post-condition**:  
- Check response status codes.
- Based on the status code display the alert message from return value.

**Flow of Events**:  
1. New User fills the form input fields.
2. Click the submit button to register.
3. Display the return message and navigate to the Login Page on success or re-register him/herself.

### 2.1.2 Login Page:
**Name**: Login Page  
**Type**: Web page  
**Description**:  
After successful registration, the user will be directed to the Login page here the user should provide two values 
1. Email (registered one) 
2. Password
After successful login, the user will be given a unique token which helps in the user’s authentication and the user will be redirected to the Chat page.  

**Operations**:  
**Name**: submitLogin()  
**API**: /auth/login/  
**Arguments**: 

|      Fields       |      type      |
|:------------------|:--------------:|
email               | string
password            | hashed_string

**Pre-condition**:   
* All fields are filled.
* Password should be of min length 8

**Returns**: 
- Message from API response.  

**Post-condition**:  
- Check response status codes.
- Based on the status code display the alert message from return value.  

**Flow of Events**:
1. User fills the form input fields.
2. Click the Login button to login.
3. Display the return message and navigate to the Chat Page on success or re-login him/herself.

### 2.1.3. Chat Page:
**Name**: Chat Page  
**Type**: Web Page  
**Description**: 
This is the page where users will be redirected after login. It will contain names of the friends the user had a  chat with. No. of unread messages will be shown beside the friend's name. Users can click on any friend's name to go to the chat thread, else they can start chatting with new friends after searching from the search bar. This page has:
1. Search Bar: To search for new/existing friends
2. Chats in left column
3. Active chat in centre and right part of the screen

**Operations**:
1. **Name**: getRecentChats()  
    **API**: /user/recentChats/  
    **Arguments**: 
    |      Fields       |      type      |
    |:------------------|:--------------:|
    userId              | ObjectId
    
    **Pre-condition**: 
    - userId is present.
    - User Token should be passed in headers of API fetch.
    
    **Returns**:
    * Message from API response, count of chats with unseen messages and list of recentChats with SenderId, SenderName, userStatus and count of unseen messages for each senderId.

    **Post-condition**:
    - Check response status codes.
    - Based on the status code display the alert message from return value.

    **Flow of Events**:
    1. User logs in successfully or reloads the page
    2. Display the Recent Chats on success.
    3. If a particular name value is passed then it will provide the friends list matches that user name.

2.  **Name**: searchFriends()  
    **API**: /user/searchFriends/  
    **Arguments**: 
    |      Fields       |      type      |
    |:------------------|:--------------:|
    searchName          | string

    **Pre-condition**: 
    - searchName field is filled.
    - User token should be passed in headers of API fetch

    **Returns**: 
    * Message from API response and searchResults
    **Post-condition**:
    - Check response status codes.
    - Based on the status code display the alert message from return value and search results.

    **Flow of Events**:
    1. User fills the search field with the name.
    2. Click the search button.
    3. Display the result in the Chat Page on success.

3.  **Name**: sendMessage / sendAttachment / editMessage / deleteMessage
    **API**: /chat/update/  
    **Arguments**:
    |      Fields       |      type      |
    |:------------------|:--------------:|
    messageId           |  ObjectId
    senderId            |  ObjectId
    receiverId          |  ObjectId
    message             |  string
    isAttachment        |  boolean
    action              |   string (NEW / EDIT / DELETE )

    **Pre-condition**: 
    - Message field is filled for sendMessage / sendAttachment / editMessage.
    - User token should be passed in headers of API fetch
    - Pass receiverId and SenderId.
    - Pass messageId for editMessage / deleteMessage.

    **Returns**: 
    * Message from API response

    **Post-condition**:  
    - Check response status codes.
    - Based on the status code display the alert message from return value.
    - Update the message list.

    **Flow of Events**:
    1. User fills the message field with a message/attachment
    2. Click the send button.
    3. On success with updated messages between a friend.

4. **Name**: getChatDetails - History of Active Chat  
    **API**: /chat/activeChat/  
    **Arguments**:
    |      Fields       |      type      |
    |:------------------|:--------------:|
    senderId            |  ObjectId
    receiverId          |  ObjectId

    **Pre-condition**: 
    - FriendId is passed.
    - User token should be passed in headers of API fetch

    **Returns**: 
    * Message from API response, friendName, friendStatus, count of unread messages, array of previousChats

    **Post-condition**:
    - Check response status codes.
    - Based on the status code display the alert message from return value if error.
    - Display friendName and friendStatus
    - On success update messages between a friend.

    **Flow of Events**:
    1. Click the particular user name from the friendList.
    2. Status will be displayed near Friend’s name.
    3. Messages will be displayed.