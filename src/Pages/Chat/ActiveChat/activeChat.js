import React, { Component } from 'react'
import './activeChat.css'
import APIService from '../../../App.service'
import { Fade } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert'
import socketClient  from "socket.io-client";
import debounce from "lodash/debounce";

class activeChat extends Component {
    constructor(props){
        super(props);
        this.socket = socketClient (process.env.REACT_APP_API_BASE_URL);
        this.state = {
          userId:localStorage.getItem('userid'),
          activeChat: [],
          selectedUser: "",
          fadeIn: null,
          alert: null,
          editMessage: '',
          isAttachment: false,
          messageContent:"",
          isTyping: false,
          me: false
        };
        this.getActiveChat=this.getActiveChat.bind(this)
        this.sendNewMessage= this.sendNewMessage.bind(this)
        this.getMessageInfo = this.getMessageInfo.bind(this)
        this.onChangeHandler = this.onChangeHandler.bind(this)
        this.updateMsgInfo = this.updateMsgInfo.bind(this)
        
    }
    onChangeHandler(e){
      this.setState({messageContent: e.target.value})
      
        this.socket.emit('typing', {
          isTyping:true,
          userId: this.state.userId,
          friendId: this.state.selectedUser.friendId
        })
      
    }

    handleTyping = debounce(function() { // continually delays setting "isTyping" to false for 500ms until the user has stopped typing and the delay runs out
    this.setState({ isTyping: false });
  }, 5000);

    async componentDidMount(){
      
      

      this.getActiveChat()
      
      this.socket.on('upto-date', (lag)=>{
        
        const {data} = lag
        if (data.length > 0){
          data.map((item)=>{
            if ((this.state.activeChat.findIndex(x => x._id === item._id) === -1 ) && ((data.senderId === this.state.selectedUser.friendId) && (data.receiverId === this.state.userId ) || (data.senderId === this.state.userId) && (data.receiverId === this.state.selectedUser.friendId ))){
            const newChats = this.state.activeChat
            newChats.push(item)
            this.setState({
              activeChat: newChats
            })
          }
          else if (this.state.activeChat.findIndex(x => x._id === item._id) !== -1 ){
            const newChats = this.state.activeChat.map((old)=>{
              return old._id === item._id ? item : old
            })
            this.setState({
              activeChat: newChats
            })
          }
        })
        }
        console.log('out',lag)
        if ((lag.senderId === this.state.userId)){
          console.log('in',lag)
          localStorage.setItem('lastTimeStamp',lag.lastTimeStamp)
        }
      })

      this.socket.on('display', (data)=>{
        if(data.isTyping==true && data.userId === this.state.selectedUser.friendId && data.friendId === this.state.userId)
         this.setState({isTyping: true}, () => {
           this.handleTyping();
         })
        else
        this.setState({isTyping: false})
      })

      this.socket.on('message', message => {
        const { action, data, lastTimeStamp } =  message
        this.setState({ isTyping : false})
        if (action === 'NEW'){
          if ((data.senderId === this.state.selectedUser.friendId) && (data.receiverId === this.state.userId ) || (data.senderId === this.state.userId) && (data.receiverId === this.state.selectedUser.friendId )){
            if (!this.state.me){
              const newChats = this.state.activeChat
              newChats.push(data)
              this.setState({
                activeChat: newChats
              })
            }
          }
          if ((data.senderId === this.state.userId))
            {
              localStorage.setItem('lastTimeStamp',lastTimeStamp)
            }
        } else if (action === 'EDIT'){
          if (this.state.activeChat.findIndex(x => x._id === data._id) !== -1 ){
            const newChats = this.state.activeChat.map((item)=>{
              return item._id === data._id ? data : item
            })
            this.setState({
              activeChat: newChats
            })
          }
          if ((data.senderId === this.state.userId))
            {
              localStorage.setItem('lastTimeStamp',lastTimeStamp)
            }
        }else if (action === 'DELETE'){
          if (this.state.activeChat.findIndex(x => x._id === data._id) !== -1 ){
            const newChats = this.state.activeChat.map((item)=>{
              return item._id === data._id ? data : item
            })
            this.setState({
              activeChat: newChats
            })
          }
          if ((data.senderId === this.state.userId))
            {
              localStorage.setItem('lastTimeStamp',lastTimeStamp)
            }
        }
      });
        
        this.socket.on('messageInfo', (data)=>{
          console.log(data)
          const newChat = this.state.activeChat.map(item => {
            if(data.messageIds.includes(item._id)){
              if(data.seenAt){
                item.seenAt = data.seenAt
              }
              if(data.deliveredAt){
                item.deliveredAt = data.deliveredAt
              }
            } 
            return item
          })
          this.setState({
            activeChat: newChat
          })
        })
    }

   
    //API to get active chat between user and friend. Called when 
    // component is mounted initially and whenever props (selected user)
    // is updated.
    async getActiveChat(){
      this.setState({selectedUser:this.props.newSelectedUser})
      const payload = {
        userId: this.state.userId,
        friendId: this.props.newSelectedUser.friendId
      }
      const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/activeChat',payload)
      const {auth, data} = result
      if (auth){
        this.setState({
          friendName: data.friendName,
          status: data.friendStatus, // !== 'Online' ? data.lastSeenAt : data.friendStatus,
          activeChat: data.previousMessages,
          friendProfilePic: data.profilePic
        })
      }
      else{
        this.setState({
          activeChat: []
        })
      }
      
    }
    
    scrollToBottom = () => {
      this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    async updateMsgInfo(messageIds,isDelivered,isSeen){
      const payload = {
        messageIds: messageIds,
        isDelivered: isDelivered,
        isSeen:isSeen
      }
      await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/updateMessageInfo',payload)
    }
    

    async componentDidUpdate(prevProps) {
      if (prevProps.newSelectedUser === null || (this.props.newSelectedUser.friendId !== prevProps.newSelectedUser.friendId)) {
          this.getActiveChat()
       }
       const messageIds = this.state.activeChat.filter(item => 
        (localStorage.getItem('userid')=== item.receiverId && !item.seenAt)? true:false
      ).map(item=>item._id)
      if(messageIds.length > 0 ){
        this.updateMsgInfo(messageIds,true,true)
      }
      this.scrollToBottom()
      
    }
         
    hideAlert() {
      this.setState({ alert: null });
    }

      
    async sendNewMessage(){
      const payload = {
        message: this.state.messageContent,
        action: "NEW",
        isAttachment: this.state.isAttachment,
        senderId: this.state.userId,
        receiverId: this.props.newSelectedUser.friendId,
        isDeleted: false,
        isEdited:false
      }
      this.setState({
        me: true
      })
      const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/update',payload)
      const {auth, msg, data, lastTimeStamp} = result
      localStorage.setItem('lastTimeStamp',lastTimeStamp)
      if (auth === true && !msg.includes('not')){
        const msgs = this.state.activeChat
        msgs.push(data)
        this.setState({
          activeChat: msgs,
          messageContent: '',
          isAttachment: false,
          me: false
        })
        const payload2 = {
          friendIds : [this.state.selectedUser.friendId],
          userId:localStorage.getItem('userid')
      }
      await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/updateDeliveredAt',payload2)
  
      }
      
    }

    convertBase64 =  (file) => {
      const resize_width = 300
      return new Promise((resolve, reject) => {
          var reader = new FileReader();

          //image turned to base64-encoded Data URI.
          reader.readAsDataURL(file);
          reader.name = file.name;//get the image's name
          reader.size = file.size; //get the image's size
          reader.onload = function(event) {
            var img = new Image();//create a image
            img.src = event.target.result;//result is base64-encoded Data URI
            img.name = event.target.name;//set name (optional)
            img.size = event.target.size;//set size (optional)
            img.onload = function(el) {
              var elem = document.createElement('canvas');//create a canvas
        
              //scale the image to 600 (width) and keep aspect ratio
              var scaleFactor = resize_width / el.target.width;
              elem.width = resize_width;
              elem.height = el.target.height * scaleFactor;
        
              //draw in canvas
              var ctx = elem.getContext('2d');
              ctx.drawImage(el.target, 0, 0, elem.width, elem.height);
        
              //get the base64-encoded Data URI from the resize image
              var srcEncoded = ctx.canvas.toDataURL(el.target, 'image/jpeg', 0);
              resolve(srcEncoded)
            }
          }
        
      })
    }

  async getFiles(e){
      const file = e.target.files[0]
      const base64 = await this.convertBase64(file)
      this.setState({ 
          messageContent: base64,
          isAttachment : true
       })
       const getAlert = () => (
        <SweetAlert 
          info
          showCancel
          cancelBtnBsStyle="light"
          focusCancelBtn={true}
          title= "Send Attachment"
          onConfirm={() => {this.sendNewMessage();this.hideAlert()}}
          onCancel={() => {
            this.hideAlert();
            this.setState({
              isAttachment: false,
              messageContent: ''
            })
          }}
        >
          <div class="attachment-preview">
            <img id="imagePreview" src={this.state.messageContent} alt=""/>
          </div>
        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
  }

    
    //API call to delete message
    async deleteMessageAction(messageId){
      const payload ={
          messageId : messageId,
          senderId: localStorage.getItem('userid'),
          receiverId: this.props.newSelectedUser.friendId,
          action: "DELETE"
      }
      const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/update',payload)
      // const result = await APIService.postAPI('http://localhost:5555/chat/update',payload)
      const {auth, msg, data, lastTimeStamp} = result
      localStorage.setItem('lastTimeStamp',lastTimeStamp)
      if (auth === true && !msg.includes('not')){
        const newChats = this.state.activeChat.map((item)=>{
          return item._id === data._id ? data : item
        })
        this.setState({
          activeChat: newChats
        })
      }
    
    }

    //Sweetalert to delete a message
    deleteNewMessage(messageId){
      const getAlert = () => (
        <SweetAlert 
          warning
          showCancel
          cancelBtnBsStyle="light"
          focusCancelBtn={true}
          title= "Delete Message"
          onConfirm={() => {this.deleteMessageAction(messageId);this.hideAlert()}}
          onCancel={() => {this.hideAlert()}}
        >
            <h4>Are you sure you want to delete this message?</h4>
        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
    }
    
    //call chat/update API with EDIT action and assigns back the response
    async editMessageAction(messageId){
      const payload ={
          messageId : messageId,
          message :  this.state.editMessage,
          senderId: localStorage.getItem('userid'),
          receiverId: this.props.newSelectedUser.friendId,
          action: "EDIT"
      }
      const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/update',payload)
      const {auth, msg, data, lastTimeStamp} = result
      localStorage.setItem('lastTimeStamp',lastTimeStamp)
      if (auth === true && !msg.includes('not')){
        const newChats = this.state.activeChat.map((item)=>{
          return item._id === data._id ? data : item
        })
        this.setState({
          activeChat: newChats
        })
      }
    
    }

    // display a SweetAlert with edit input tag to get the new Message value
    editNewMessage(messageId){
      const previousMessage  = this.state.activeChat.filter((item)=> item._id=== messageId)[0]
      const getAlert = () => (
        <SweetAlert 
          warning
          showCancel
          cancelBtnBsStyle="light"
          focusCancelBtn={true}
          title= "Edit Message"
          onConfirm={() => {this.editMessageAction(messageId);this.hideAlert()}}
          onCancel={() => {this.hideAlert()}}
        >
            <h4>Message:</h4>
            <input name="editMessage" id="editMessage" defaultValue={previousMessage.message} onChange={(event) => {this.setState({editMessage: event.target.value})}} placeholder='Enter New Message' class="form-control"/>
        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
    }

    getMessageInfo(message){
      const getAlert = () => (
        <SweetAlert 
          info
          title= "Message Info"
          onConfirm={() => {this.hideAlert()}}
        >
          <h5>Sent Time : {new Date(message.sentAt).toLocaleString(undefined, {timeZone: 'Asia/Kolkata'})}</h5>
          <h5>Delivered Time : {message.deliveredAt? new Date(message.deliveredAt).toLocaleString(undefined, {timeZone: 'Asia/Kolkata'}) : null}</h5>
          <h5>Seen Time : {message.seenAt?new Date(message.seenAt).toLocaleString(undefined, {timeZone: 'Asia/Kolkata'}): null}</h5>
        </SweetAlert>
      );
      this.setState({
        alert: getAlert()
      });
    }

    toggle(value){
      this.setState({
      fadeIn: this.state.fadeIn !== value ? value: null
    });
  }
      
    render() {   
        return (        
            <div id="frame">
              <div class="content">
                <div class="contact-profile">
                  <img src={this.state.friendProfilePic} alt="" />
                  <p>{this.state.friendName} </p> 
                  {this.state.isTyping === true ?
                  <p>   ....is typing</p>
                  :
                  <p></p>
                }
                  
                </div>
                <div class="messages">
                  <ul>
                  {
                    this.state.activeChat.map((item,key)=>{
                    if (this.state.userId === item.receiverId && item.isDeleted === false){
                      return (
                        <li key={key} class="sent">
                          <img className="profile" src={this.state.friendProfilePic} alt="" />
                          {item.isAttachment ? <img style={{float:'left'}} className="attachment" src={item.message}alt=""/> : <p>{item.message}</p>}
                        </li>
                    )
                    }
                    else{
                      if(item.isDeleted === false){
                      return (
                          <li key={key} class="replies" onClick={()=>this.toggle(key)}>
                            <img className="profile" src={localStorage.getItem('profilePic')} alt="" />
                            {item.isAttachment ? <img style={{float:'right'}} className="attachment" src={item.message} width="600px" height="300px" alt=""/> : 
                            <p>
                              {item.message} <span style={{marginLeft:'10px'}}>{item.isEdited? <i class="fa fa-pencil " aria-hidden="true"></i> : ''}</span>
                            </p>}
                            <Fade in={this.state.fadeIn===key} style={{float:'right'}}>
                                  <button type="button" class="btn-secondary btn-sm" onClick={()=>this.editNewMessage(item._id)} ><i class="fa fa-pencil " aria-hidden="true"></i> edit</button>
                                  <button type="button" class="btn-danger btn-sm" onClick={()=>this.deleteNewMessage(item._id)}><i class="fa fa-trash " aria-hidden="true"></i> delete</button>
                                  <button type="button" class="btn-info btn-sm" onClick={() => this.getMessageInfo(item)}><i class="fa fa-info "></i> info</button>
                            </Fade>
                            
                          </li>
                      )
                    }
                    
                  }})
                  }
                    
                  </ul>
                  <div style={{ float:"left", clear: "both" }}
                    ref={(el) => { this.messagesEnd = el; }}>
                </div>

                </div>
                <div class="message-input">
                  <div class="wrap">
                  <input type="text" placeholder="Write your message..." name="messageContent" value = {this.state.messageContent} onChange={ this.onChangeHandler}/>
                  <label for="file"><span class='fa fa-2x fa-paperclip'> </span></label>
                  {/* <input type="file" id="file" style={{display: "none"}}/> */}
                  <input type='file' id="file" style={{display: "none"}} onChange={this.getFiles.bind(this) }/>
                  {/* <i class="fa fa-paperclip attachment"></i> */}
                  <button class="submit" onClick = {() => this.sendNewMessage()}><i class="fa fa-paper-plane"></i></button>
                  </div>
                </div>
              </div>
              {this.state.alert}
            </div> 
            )
    }
}

export default activeChat
