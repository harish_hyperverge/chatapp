import React, {Component} from 'react'
import './RecentChat.css'
import ChatHttpServer from '../../../utils/ChatHttpServer'
import ChatSocketServer from '../../../utils/ChatSocketServer'
import APIService from '../../../App.service'
import socketClient  from "socket.io-client";
class RecentChat extends Component{
    userId = null;
    constructor(props) {
        super(props);
        this.socket = socketClient (process.env.REACT_APP_API_BASE_URL)
        this.state = {
            lastTimeStamp: "",
            totalNewChats: "",
            recentChats: [],
            input:"",
            searchList: [],
            selectedUserId: ""
        }
        this.updateDeliveredAt = this.updateDeliveredAt.bind(this)
      }
    
      
    async componentDidMount() {
        this.userId = await ChatHttpServer.getUserId()
        ChatSocketServer.establishSocketConnection(this.userId)
        const chatListResponse = await ChatHttpServer.getRecentChats(this.userId)
        localStorage.setItem('lastTimeStamp', chatListResponse.lastTimestamp)
        this.socket.emit('recentChat', {
            userId: this.userId
        });
        this.socket.on('recentChat-response', (data) => {
            console.log("listening recent",data)
            if(data.userId === this.userId){
                const chats = data.recentChats
            // chats = chats.filter(function (obj) {
            //     return obj.friendId !== data.recentChats[0].friendId ? data.recentChats[0]: obj;
            // });
                this.setState({
                    recentChats: chats,
                });
                this.updateDeliveredAt()
            }
            
        });
        this.socket.on('message', (data) => {
            this.socket.emit('recentChat', {
                userId: this.userId
            });
        });

        this.setState({
            recentChats: chatListResponse.data.recentChats,
            totalNewChats:chatListResponse.data.totalNewChats,
            lastTimeStamp: chatListResponse.lastTimestamp
          });
        this.updateDeliveredAt()
        let chats=this.state.recentChats
        console.log(this.props.friendId)
        // chats = chats.filter(function (obj) {
        //     return obj.friendId === this.props.friendId ? obj.unreadCount==0 : obj;
        // });
        console.log(chats)
    }

    async updateDeliveredAt(){
        const friends = this.state.recentChats.map(item => item.friendId)
        console.log(friends)
        const payload = {
            friendIds : friends,
            userId:localStorage.getItem('userid')
        }
        const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/chat/updateDeliveredAt',payload)

    }
    async onChangeHandler(e){
        this.setState({
          input: e.target.value,
          searchList: e.target.value !==''?await ChatHttpServer.getSearchList(e.target.value): [],
        })
       // e.target.value=''
    }
    // setFriendId(id){
    //     localStorage.setItem('friendId',id);
    //     //window.location.href='/chat/activeChat'
    // }
   
    selectedUser = (user) => {
        this.setState({
          selectedUserId: user.friendId,
          searchList:[]
        });
        this.props.updateSelectedUser(user)
    }

    render(){
        const list = this.state.searchList
        return(
            <div class="card mb-sm-3 mb-md-0 contacts_card">
                <div class="card-header">
                    <div class="input-group">
                        <input id="search" type="text" placeholder="Search..." name="" class="form-control search" onChange={this.onChangeHandler.bind(this)} ></input>
                        <div class="input-group-prepend">
                            <span class="input-group-text search_btn">
                                <i class="fa fa-search"></i></span>
                        </div>
                        
                    </div>
                    <ul>
                        {list.map((data, key) => {
                        return (
                            
                            <div key = {key} class="d-flex bd-highlight">
								<div class="img_cont">
									<img src={data.profilePic} class="rounded-circle user_img" alt=""/>
                                    {/* <img src="https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg" class="rounded-circle user_img"></img> */}
                                    <span class={data.friendStatus === true ? "online_icon" :"online_icon offline" }></span>
								</div>
                                <div class="d-flex justify-content-between">
								<div class="user_info ">
									<span>{data.firstName} {data.lastName}   </span>
									 {data.friendId !== this.props.friendId && data.unreadCount > 0 ? (
                                        <span className="room-unread" >{data.unreadCount}</span>
                                    ): null}
                                     <button type="button" class="btn btn-sm"  onClick={()=>{this.selectedUser(data); document.getElementById("search").value=''}}>Chat Now</button><hr />
								</div>
                                </div>
							</div>
                            
                            )
                        })
                    }
                    </ul>
                </div>
                <div class="card-body contacts_body">
                    <ui class="contacts">   
                    {this.state.recentChats.map((data,key) => {
                        return(
                            <li key={key} class={this.state.selectedUserId === data.friendId ? 'active' : ''} onClick={() => this.selectedUser(data)}>
							<div class="d-flex bd-highlight">
								<div class="img_cont">
									<img src={data.profilePic} class="rounded-circle user_img" alt=""/>
                                    {/* <img src="https://2.bp.blogspot.com/-8ytYF7cfPkQ/WkPe1-rtrcI/AAAAAAAAGqU/FGfTDVgkcIwmOTtjLka51vineFBExJuSACLcBGAs/s320/31.jpg" class="rounded-circle user_img"></img> */}
                                    <span class={data.friendStatus === true ? "online_icon" :"online_icon offline" }></span>
								</div>
                                <div class="d-flex justify-content-between">
								<div class="user_info ">
									<span>{data.firstName} {data.lastName}   </span>
                                     { 
                                     data.friendId !== this.props.friendId  && data.unreadCount !== 0? (
                                        <span className="room-unread" style={{"float":"right"}}>{data.unreadCount}</span>
                                    ): null }
								</div>
                                </div>
							</div>
						    </li>
                        )
                    })}    
                    
						
                    </ui>
                </div>
                <div class="card-footer">
                </div>
            </div>
        )
    }
}
export default RecentChat