import {Component} from 'react'
import RecentChat from './RecentChat/RecentChat'
import ActiveChat from './ActiveChat/activeChat'
class Chat extends Component{
    constructor(props) {
        super(props);
        this.state = {
            selectedUser: "",
            friendId:""
        };
        this.updateSelectedUser = this.updateSelectedUser.bind(this)
      }
    updateSelectedUser = (user) => {
        this.setState({
          selectedUser: user,
          friendId: user.friendId
        })       
      }
    render(){
        return(
            <div className="row">
                    <div className="col-4">
                        <RecentChat friendId={this.state.friendId} updateSelectedUser={this.updateSelectedUser}/>
                    </div>
                    {this.state.selectedUser ? 
                    <div className="col-8">
                        
                        <ActiveChat newSelectedUser={this.state.selectedUser}/>
                    </div>
                    :
                    <div className="col-8"></div>
                    }
            </div>
        )
    }
}
export default Chat