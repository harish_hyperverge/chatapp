import React, { Component } from 'react'
import './auth.css'
import APIService from '../../App.service'
import SweetAlert from 'react-bootstrap-sweetalert'
// import FileBase64 from 'react-file-base64'
import $ from 'jquery';

const CryptoJS = require("crypto-js");



class auth extends Component {
    constructor(props){
        super(props);
        this.state = {
            loginEmail: '',
            loginPassword: '',
            password: '',
            email: '',
            phone: '',
            firstName: '',
            lastName: '',
            profilePic: '',
            alert : null,
            imgback: 'https://png.pngtree.com/png-vector/20191009/ourmid/pngtree-user-icon-png-image_1796659.jpg'
        }
        
        window.onload=function(){
            const sign_in_btn = document.querySelector("#sign-in-btn");
            const sign_up_btn = document.querySelector("#sign-up-btn");
            const container = document.querySelector(".auth-container");

            sign_up_btn.addEventListener("click", () => {
                container.classList.add("sign-up-mode");
            });

            sign_in_btn.addEventListener("click", () => {
                container.classList.remove("sign-up-mode");
            });
        }
        this.submitLogin = this.submitLogin.bind(this);
        this.submitRegister = this.submitRegister.bind(this);

    }

    hideAlert() {
        this.setState({ alert: null });
    }

    async submitRegister(e){
        e.preventDefault();
        const bodyData = {
            email: this.state.email,
            password : CryptoJS.SHA3(this.state.password).toString(),
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            phone : this.state.phone,
            profilePic: this.state.profilePic
        }
        const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/auth/register',bodyData)
        const {msg} = result
        if (msg.includes('successfully')){
            const getAlert = () => (
                <SweetAlert 
                    success
                    title= "Message"
                    onConfirm={() => {this.hideAlert();window.location.href = '/'}}
                >
                    {msg}
                </SweetAlert>
            );
                    
            this.setState({
            alert: getAlert()
            })
        } else {
            const getAlert = () => (
                <SweetAlert 
                    error 
                    title= "Message"
                    onConfirm={() => this.hideAlert()}
                >
                    {msg}
                </SweetAlert>
            );
            
            this.setState({
            alert: getAlert()
            })
        }        
    };

    async resizeBase64Img(dataUrl,newWidth){
        let image, oldWidth, oldHeight, newHeight, canvas, ctx;
        return (new Promise(function(resolve){
          image = new Image(); 
          image.src = dataUrl;
          oldWidth = image.width; oldHeight = image.height;
          newHeight = Math.floor(oldHeight / oldWidth * newWidth);
          canvas = document.createElement("canvas");
          canvas.width = newWidth; 
          canvas.height = newHeight;
          ctx = canvas.getContext("2d");
          ctx.drawImage(image, 0, 0, newWidth, newHeight);
          resolve(canvas.toDataURL());
        })); 
    }

    convertBase64 =  (file) => {
        const resize_width = 300
        return new Promise((resolve, reject) => {
            var reader = new FileReader();

            //image turned to base64-encoded Data URI.
            reader.readAsDataURL(file);
            reader.name = file.name;//get the image's name
            reader.size = file.size; //get the image's size
            reader.onload = function(event) {
              var img = new Image();//create a image
              img.src = event.target.result;//result is base64-encoded Data URI
              img.name = event.target.name;//set name (optional)
              img.size = event.target.size;//set size (optional)
              img.onload = function(el) {
                var elem = document.createElement('canvas');//create a canvas
          
                //scale the image to 600 (width) and keep aspect ratio
                var scaleFactor = resize_width / el.target.width;
                elem.width = resize_width;
                elem.height = el.target.height * scaleFactor;
          
                //draw in canvas
                var ctx = elem.getContext('2d');
                ctx.drawImage(el.target, 0, 0, elem.width, elem.height);
          
                //get the base64-encoded Data URI from the resize image
                var srcEncoded = ctx.canvas.toDataURL(el.target, 'image/jpeg', 0);
                resolve(srcEncoded)
              }
            }
          
        })
      }

    async getFiles(e){
        const file = e.target.files[0]
        const base64 = await this.convertBase64(file)
        this.setState({ 
            profilePic: base64,
            imgback: base64
         })
    }

    async submitLogin(e){
        e.preventDefault()
        const {loginEmail, loginPassword} =this.state
        const result = await APIService.postAPI(process.env.REACT_APP_API_BASE_URL+'/auth/login',{email: loginEmail, password: CryptoJS.SHA3(loginPassword).toString()})
        if (result.auth === true){
            localStorage.setItem('token',result.token)
            localStorage.setItem('userid',Object(result.data.loggedData.userId))
            localStorage.setItem('profilePic',result.data.loggedData.profilePic)
            const getAlert = () => (
                <SweetAlert 
                    success
                    title= "Message"
                    onConfirm={() => {this.hideAlert();window.location.href = '/chat'}}
                >
                    Success
                </SweetAlert>
            );
                    
            this.setState({
            alert: getAlert()
            })
        }else {
            const getAlert = () => (
                <SweetAlert 
                    error 
                    title= "Message"
                    onConfirm={() => this.hideAlert()}
                >
                    {result.msg}
                </SweetAlert>
            );
            
            this.setState({
            alert: getAlert()
            })
        } 
    }
    render() {
        return (
            <div class="auth-container">
                <div class="forms-container">
                    <div class="signin-signup">
                    <form onSubmit={this.submitLogin} class="sign-in-form">
                        <h2 class="title">Sign in</h2>
                        <div class="input-field">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <input type="email" placeholder="Email" name="loginEmail" onChange={(event) => this.setState({loginEmail: event.target.value})}/>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-key" aria-hidden="true"></i>
                            <input type="password" placeholder="Password" name="loginPassword" onChange={(event) => this.setState({loginPassword: event.target.value})}/>
                        </div>
                        <input type="submit" value="Login" class="btn solid"/>
                    </form>
                    <form onSubmit={this.submitRegister} class="sign-up-form">
                        <h2 class="title">Sign up</h2>
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" onChange={this.getFiles.bind(this) } />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <img id="imagePreview" src={this.state.imgback} alt=""/>
                            </div>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" placeholder="First Name" name="firstName" onChange={(event) => this.setState({firstName: event.target.value})}/>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" placeholder="Last Name" name="lastName" onChange={(event) => this.setState({lastName: event.target.value})}/>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <input type="text" placeholder="Phone" name="phone" onChange={(event) => this.setState({phone: event.target.value})}/>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <input type="email" placeholder="Email" name="email" onChange={(event) => this.setState({email: event.target.value})}/>
                        </div>
                        <div class="input-field">
                            <i class="fa fa-key" aria-hidden="true"></i>
                            <input type="password" placeholder="Password" name="password" onChange={(event) => this.setState({password: event.target.value})}/>
                        </div>
                        {/* <div class="input-field">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                            <FileBase64 id="profilePic" name="profilePic" required onDone={this.getFiles.bind(this) } />
                        </div> */}
                        <input type="submit" class="btn" value="Sign up" />
                    </form>
                    </div>
                </div>

                <div class="panels-container">
                    <div class="panel left-panel">
                    <div class="content">
                        <h3>New here ?</h3>
                        <p>
                        Welcome to our Chat application please register yourself to avail our service.
                        </p>
                        <button class="btn transparent" id="sign-up-btn">
                        Sign up
                        </button>
                    </div>
                    <img src="https://raw.githubusercontent.com/sefyudem/Sliding-Sign-In-Sign-Up-Form/955c6482aeeb2f0e77c1f3c66354da3bc4d7a72d/img/log.svg" class="image" alt="" />
                    </div>
                    <div class="panel right-panel">
                    <div class="content">
                        <h3>One of us ?</h3>
                        <p>
                        Happy to see you again please login yourself to start your Chats..
                        </p>
                        <button class="btn transparent" id="sign-in-btn">
                        Sign in
                        </button>
                    </div>
                    <img src="https://raw.githubusercontent.com/sefyudem/Sliding-Sign-In-Sign-Up-Form/955c6482aeeb2f0e77c1f3c66354da3bc4d7a72d/img/register.svg" class="image" alt="" />
                    </div>
                </div>
                {this.state.alert}
            </div>
        )
    }
}

export default auth