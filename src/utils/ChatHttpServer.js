import * as axios from 'axios'

class ChatHttpServer {

    getRecentChats(userId){
       return new Promise( async (resolve,reject) => {
           try{
             const token =  {
                 headers :{
                    'x-access-token':localStorage.getItem('token')
                 }
             }
             const response = await axios.post('http://localhost:5555/user/recentChats',{userId}, token)
            resolve(response.data);
           }catch(error){
               reject(error)
           }
       })
    }

    getSearchList(name){
        return new Promise( async (resolve,reject) => {
            try{
              const httpOption =  {
                  headers :{
                     'x-access-token':localStorage.getItem('token'),
                     'lastTimestamp' : localStorage.getItem('lastTimeStamp')
                  }
              }
              const response = await axios.get('http://localhost:5555/user/searchFriends/'+name, httpOption)   
             
             const {auth, data} = response.data
            if (auth === true){
                resolve(data);
            }
            else{
                resolve([]);
            }
            }catch(error){
                reject(error)
            }
        })
     }

    getUserId() {
        return new Promise((resolve, reject) => {
            try {
                resolve(localStorage.getItem('userid'))
            } catch (error) {
                reject(error)
            }
        })
    }
}
export default new ChatHttpServer()