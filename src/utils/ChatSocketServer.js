import * as io from 'socket.io-client';
const events = require('events');


class ChatSocketServer {
    
    socket = null
    eventEmitter = new events.EventEmitter()

    // Connecting to Socket Server
    establishSocketConnection(userId) {
        try {
            this.socket = io('http://localhost:5555', {
                query: `userId=${userId}`
            });
            // this.socket.io('connection',{
            //     userId:userId
            // })
        } catch (error) {
            alert('Something went wrong; Cant connect to socket server')
        }
    }
    
    //Socket to emit and listen recentchats
    getRecentChats(userId){
        return new Promise( async (resolve,reject) => {
        try{
            this.socket.emit('recentChat', {
                userId: userId
            });
            this.socket.on('recentChat-response', (data) => {
                this.eventEmitter.emit('recentChat-response', data);
                resolve(data)
            });
        }catch(error){
            alert('Something went wrong; Cant connect to socket server')
            reject(error)
        }
    })
    }

   
  

}
export default new ChatSocketServer()