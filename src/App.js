import './App.css'
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Chat from './Pages/Chat/Chat'
import Auth from './Pages/Auth/auth'

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/chat' component={Chat}></Route>
        <Route path='/' component={Auth}></Route>
        {/* <Route path='/chat/activeChat' component={Chat}></Route> */}
      </Switch>
    </Router>
    
  );
  }

export default App
